#include "oodle.h"


CBarndoorsTransition::CBarndoorsTransition(const CSurface &a, const CSurface &b)
                      : leftDoor(CVector(-10,0), CVector(0,0), a.getSection(0,0,a.getWidth()/2, a.getHeight()), "static"),
                        rightDoor(CVector(10,0), CVector(a.getWidth()/2,0), a.getSection(a.getWidth()/2,0,a.getWidth()/2, a.getHeight()), "static"),
                        b(b)
{ }

void CBarndoorsTransition::update()
{
	leftDoor.update();
	rightDoor.update();
}

void CBarndoorsTransition::draw(CSurface &dest)
{
	leftDoor.draw(dest);
	rightDoor.draw(dest);
}

bool CBarndoorsTransition::done()
{
	return (leftDoor.getPos().getX() < leftDoor.getWidth()) && (rightDoor.getPos().getX() > 2*rightDoor.getWidth());
}
	