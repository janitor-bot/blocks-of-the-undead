#include "CGotoEvent.h"

CGotoEvent::CGotoEvent(CParticle &o, CVector dest, double speed, bool c) 
                       : CSequencedEvent(c), obj(o), dest(dest), start(ZERO_VECTOR), speed(speed)
{

}

CGotoEvent* CGotoEvent::getCopy()
{
	return new CGotoEvent(*this);
}

CGotoData* CGotoEvent::getEventData()
{
	int dtype = 0;
	if (start.getY() < dest.getY())
	{
		dtype = dtype | static_cast<int> (CGotoData::BOTTOM);
	}
	else if (start.getY() > dest.getY())
	{
		dtype |= static_cast<int> (CGotoData::TOP);
	}

	if(start.getX() < dest.getX())
	{
		dtype |= static_cast<int> (CGotoData::RIGHT);
	}
	else if (start.getX() > dest.getX())
	{
		dtype |= static_cast<int> (CGotoData::LEFT);
	}
	return new CGotoData(dtype);
}

void CGotoEvent::initialize()
{
	CSequencedEvent::initialize();

	start = obj.getPos();
	CVector vel = (dest - start);
	vel.setMag(speed);
	obj.setVelocity( vel );
}

void CGotoEvent::doUpdate()
{
	if (!isRunning())
	{
		return;
	}

	bool valY = false, valX = false;

	// first check if it is at or past dest.y (check greater than if it is going down, check less than if going up)
	if (obj.getVel().getY() < 0)
	{
		if (obj.getPos().getY() <= dest.getY())
		{
			valY = true;
		}
	}
	else
	{
		if (obj.getPos().getY() >= dest.getY())
		{
			valY = true;
		}
	}

	// check if it is at or past dest.x (check greater than if it is going right, check less than if going left)
	if (obj.getVel().getX() < 0)
	{
		if (obj.getPos().getX() <= dest.getX())
		{
			valX = true;
		}
	}
	else
	{
		if (obj.getPos().getX() >= dest.getX())
		{
			valX = true;
		}
	}
	
	if (valX && valY)
	{
		obj.setVelocity(ZERO_VECTOR);
		obj.setPos(dest);
		setDone();
	}
}