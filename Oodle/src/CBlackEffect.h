#ifndef _CBLACKEFFECT_
#define _CBLACKEFFECT_

#include "CEffectSurface.h"

class CBlackEffect : public CEffect
{
public:
	CBlackEffect(int duration, bool concurrent, const CSurface &fx);
	CBlackEffect* getCopy();
	void doUpdate() {}
	bool hasDraw() { return true; }
	void draw(CSurface &s, int x, int y);


private:
	int duration;
	CSurface black;
	const CSurface &src;
};

#endif