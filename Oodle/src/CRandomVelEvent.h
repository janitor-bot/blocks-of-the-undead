#ifndef _CRANDOMVELEVENT_
#define _CRANDOMVELEVENT_

#include "CParticle.h"
#include "CTimedEvent.h"

class CRandomVelEvent : public CTimedEvent
{
public:
	CRandomVelEvent(CParticle &p, double prob, int period, double changeX, double changeY, bool c) 
		            : CTimedEvent(period, c), p(p), prob(prob), changeX(changeX), changeY(changeY) { }
	
	void doUpdate();

	CRandomVelEvent* getCopy() { return new CRandomVelEvent(*this); }

private:
	double prob;
	double changeX, changeY;
	CParticle &p;
};

#endif