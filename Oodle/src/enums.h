#ifndef _ENUMS_
#define _ENUMS_

enum DestType
{
	NA = 0,
	TOP = 2,
	BOTTOM = 4,
	LEFT = 8,
	RIGHT = 16
};


#endif