#ifndef _CREVERSEBLACKEFFECT_
#define _CREVERSEBLACKEFFECT_

#include "CEffectSurface.h"

class CReverseBlackEffect : public CEffect
{
public:
	CReverseBlackEffect(int duration, bool concurrent, const CSurface &fx);
	CReverseBlackEffect* getCopy();
	void doUpdate() {}
	bool hasDraw() { return true; }

	void draw(CSurface &s, int x, int y);

private:
	int duration;
	CSurface black;
	const CSurface &src;
};

#endif