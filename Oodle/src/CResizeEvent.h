#ifndef _CRESIZEEVENT_
#define _CRESIZEEVENT_

#include "CSprite.h"
#include "CTimedEvent.h"
#include "SDL_rotozoom.h"

class CResizeEvent : public CTimedEvent
{
public:
	CResizeEvent(CSprite &s, double minScale, double maxScale, int time, bool c) : CTimedEvent(0, c), s(s), minScale(minScale), maxScale(maxScale), time(time) {}
	CResizeEvent* getCopy() { return new CResizeEvent(*this); }

	void initialize() { CTimedEvent::initialize(); s.setScale(minScale); }
	void doUpdate();

private:
	CSprite &s;
	double minScale;
	double maxScale;
	unsigned int time;
};

#endif