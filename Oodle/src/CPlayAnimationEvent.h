#ifndef _CPLAYANIMATIONEVENT_
#define _CPLAYANIMATIONEVENT_

#include "CSequencedEvent.h"
#include "CSprite.h"

class CPlayAnimationEvent : public CSequencedEvent
{
public:
	CPlayAnimationEvent(CSprite &s, const string &anim, bool concurrent = false) : CSequencedEvent(concurrent), s(s), anim(anim) {}
	
	void initialize() { s.setAnimation(anim); }
	void doUpdate() { if (s.getCurrentAnimation()->isDone()) setDone(); }
	CPlayAnimationEvent* getCopy() { return new CPlayAnimationEvent(*this); }

private:
	CSprite &s;
	string anim;
};

#endif