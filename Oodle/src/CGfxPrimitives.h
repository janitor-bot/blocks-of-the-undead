#ifndef _CGFXPRIMITIVES_
#define _CGFXPRIMITIVES_

#include "CColor.h"
#include "CSurface.h"

class CSurface;

class CGfxPrimitives
{
public:
	
	static void drawCircle(int x, int y, int r, CColor &c, CSurface &dest, bool filled = true);
};

#endif