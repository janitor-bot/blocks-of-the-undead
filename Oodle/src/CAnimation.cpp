#include "oodle.h"
#include <set>
using namespace std;

CAnimation::CAnimation(const string &fileName, 
					   int width, int height, int padding, int speed, bool alpha, bool nopath)
						: width(width), height(height), isClone(false), animating(true),
						  timer(speed), done(false), loop(true)
{
	frames = CImgLoader::loadBigassBitmap(fileName, width, height, padding, alpha, nopath);
	currentFrame = frames.begin();
}

CAnimation::CAnimation(const string &fileName, 
					   int width, int height, int padding, int speed, bool loop, bool alpha, bool nopath)
						: width(width), height(height), isClone(false), animating(true),
						  timer(speed), done(false), loop(loop)
{
	frames = CImgLoader::loadBigassBitmap(fileName, width, height, padding, alpha, nopath);
	currentFrame = frames.begin();
}


CAnimation::CAnimation(const string &fileName, bool alpha, bool nopath)
						: isClone(false), animating(false), timer(0),
                          done(false), loop(true)
{
	frames.push_back(CImgLoader::loadImage(fileName, alpha, nopath));
	currentFrame = frames.begin();
	width = (*currentFrame)->getWidth();
	height = (*currentFrame)->getHeight();
}

CAnimation::CAnimation(CSurface* s)
						: isClone(false), animating(false), timer(0),
                          done(false), loop(true)
{
	if (!s)
	{
		_THROWEX(ex_illegalArguments, "Surface 's' is a NULL pointer", "CAnimation", "CAnimation", "s = {" << s << "}");
	}

	frames.push_back(s);
	currentFrame = frames.begin();
	width = (*currentFrame)->getWidth();
	height = (*currentFrame)->getHeight();
}

CAnimation::CAnimation(const list<CSurface*> &s, int period, bool loop)
						: isClone(false), animating(true), timer(period),
                          done(false), loop(loop)
{
	for(list<CSurface*>::const_iterator i = s.begin(); i != s.end(); ++i)
	{
		if(*i == NULL)
		{
			_THROWEX(ex_illegalArguments, "Given list of Surfaces contains NULL ptr", "CAnimation", "CAnimation", "s = {" << &s << "}");
		}
		
		frames.push_back(*i);
		currentFrame = frames.begin();
		width = (*currentFrame)->getWidth();
		height = (*currentFrame)->getHeight();
	}
}

const CAnimation& CAnimation::operator=(const CAnimation &a)
{
	if (&a != this)   // don't allow assignment to self
	{
		// free old memory
		list<CSurface*>::iterator i;
		for(i = frames.begin(); i != frames.end(); ++i)
		{
			delete *i;
		}
		frames.clear();

		for(i = frames.begin(); i != frames.end(); ++i) {
			frames.push_back( new CSurface(**i) );
		}
		
		currentFrame = frames.begin();   // not sure how to preserve the currentFrame so do this

		animating = a.animating;
		isClone = a.isClone;
		timer = a.timer;
		loop = a.loop;
		done = a.done;
	}
	return *this;
}

CAnimation::~CAnimation()
{
	if ((isClone == false))
	{
		set<CSurface*> s;
		s.insert(frames.begin(), frames.end());
		set<CSurface*>::iterator i;
		for(i = s.begin(); i != s.end(); ++i) {
			delete (*i);
		}
		frames.clear();
	}
}

// copy constructor
CAnimation::CAnimation(const CAnimation& obj)
	                    : width(obj.width), height(obj.height), animating(obj.animating),
						  isClone(obj.isClone), timer(obj.timer), done(obj.done), loop(obj.loop)
{
	list<CSurface*>::iterator i;
	for(i = frames.begin(); i != frames.end(); ++i) {
		frames.push_back( new CSurface(**i) );
	}
	
	currentFrame = frames.begin();   // not sure how to preserve the currentFrame so do this

}

CAnimation* CAnimation::getClone()
{
	CAnimation* clone = new CAnimation(*this, true);
	return clone;
}

CAnimation::CAnimation(const CAnimation& obj, bool clone)
	                    : width(obj.width), height(obj.height), animating(obj.animating),
						  frames(obj.frames), isClone(clone), timer(obj.timer), done(obj.done), loop(obj.loop)
{
	if (!clone)
	{
		_THROWEX(ex_illegalArguments, "Clone constructor called without intent to clone", "CAnimation", "CAnimation", "sprite = {" << &obj << "}, clone = " << clone);
	}
	
	currentFrame = frames.begin();
	timer=  obj.timer;
}

void CAnimation::setAlpha(Uint32 flags, Uint8 alpha)
{
	list<CSurface*>::iterator i;
	for(i = frames.begin(); i != frames.end(); ++i) {
		(*i)->setAlpha(flags, alpha);
	}
}	

void CAnimation::update()
{
	if (timer.isRinging() && animating) {  // if speed seconds have been spent on current frame

		if(++currentFrame == frames.end())     // make sure we are not overstepping list
		{
			if (loop)
			{
				currentFrame = frames.begin();
			}
			else
			{
				currentFrame = frames.end();
				animating = false;
				done = true;
			}
		}
	}
}

int CAnimation::getFrames()
{
	/*int n = 0;
	for (list<CSurface*>::iterator i = frames.begin(); i != frames.end(); ++i)
	{
		n++;
	}
	return n;*/
	return frames.size();
}

void CAnimation::setFrame(int n)
{
	int i2 = 0;
        list<CSurface*>::iterator i;

	for (i = frames.begin();  i != frames.end(); ++i)
	{if (i2 >= n) break; i2++;}
	currentFrame = i;
}

int CAnimation::getFrameIndex()
{
	int i2 = 0;
	for (list<CSurface*>::iterator i = frames.begin();  i != frames.end(); ++i)
	{
		if (i == currentFrame) break; i2++;
	}
	return i2;
}
