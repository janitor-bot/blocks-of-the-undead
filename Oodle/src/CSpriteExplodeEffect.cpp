#include "oodle.h"

CSpriteExplodeEffect::CSpriteExplodeEffect(CSprite &s, int duration, double minVel, double randVel, const CRect &r, int xPieces, int yPieces, bool concurrent)
	: CExplodeEffect(s, duration, minVel, randVel, r, concurrent), xPieces(xPieces), yPieces(yPieces)
{
}

CSpriteExplodeEffect::CSpriteExplodeEffect(const CSpriteExplodeEffect &e)
       : CExplodeEffect(e)
{
	for (list<CParticle*>::iterator i = pieces.begin(); i != pieces.end(); ++i)
	{
		pieces.push_back( new CSprite(*((CSprite*)(*i))) );
	}
}

CExplodeEffect *CSpriteExplodeEffect::getCopy()
{
	return new CSpriteExplodeEffect(*this);
}

void CSpriteExplodeEffect::initialize()
{
	CEffect::initialize();
	/*int xPieces = 4;
	int yPieces = 4;*/
	int sHeight = s.getCurrentFrame()->getHeight();
	int sWidth = s.getCurrentFrame()->getWidth();
	int xSzPiece = sWidth / xPieces;
	int ySzPiece = sHeight / yPieces;
    CVector center = CVector(sWidth / xSzPiece, sHeight / ySzPiece);

	for (int x = 0; x < sWidth; x = min(sWidth, x + xSzPiece) )
	{
		for (int y = 0; y < sHeight; y = min(sHeight, y + ySzPiece) )
		{
			CVector p(x,y);
			CVector vel = (p - center);
			vel.setMag(randVel*(rand()%50/50.)+minVel);
			vel.setDir( vel.getDirRad() + (rand()%50/50.) * (rand()%50/50. > .5 ? -1 : 1));

			CSprite* sprite = new CSprite(vel, CVector(x,y), s.getCurrentFrame()->getSection(x, y, min(sWidth - x, xSzPiece), min(sHeight - y, ySzPiece)), "static");
			sprite->setRect(s.getRect());
			//cout << "width = " << min(sWidth - x, xSzPiece) << ", height = " << min(sHeight - y, ySzPiece) << endl;
			//cout << "x: " << x << " y: " << y << endl;
			pieces.push_back( sprite );
			numPieces++;
		}
	}
}

void CSpriteExplodeEffect::doUpdate()
{
	for (list<CParticle*>::iterator i = pieces.begin(); i != pieces.end(); ++i)
	{
		unsigned int t = ticksElapsed();
		if (t > 1000) { setDone(); return; }

		((CSprite*)(*i))->setScale( (1000-t) / (1000.) );
		((CSprite*)(*i))->setRot( (1000-t) / (1000.) * 2 * PI);
		(*i)->update();
	}
	if (gone >= numPieces) {
		setDone();
	}
}
