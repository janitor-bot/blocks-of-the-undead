#ifndef _CBOUNDS_
#define _CBOUNDS_

#include "CRect.h"

class CBounds : public CRect
{
public:
	CBounds(double x, double y, double width, double height, CRect::Side flag = CRect::NONE);
	bool contains(double x, double y);
	bool checkLeft(double x);
	bool checkRight(double x);
	bool checkTop(double y);
	bool checkBottom(double y);

private:
	CRect::Side flag;
};

#endif