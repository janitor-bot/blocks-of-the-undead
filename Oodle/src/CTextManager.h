#ifndef _CTEXTMANAGER_
#define _CTEXTMANAGER_

#include <string>
using namespace std;

class CTextManager
{
public:
	enum FontType
	{
		PLAIN,
		FANCY
	};

	static string getDefaultFont(FontType t = PLAIN);
	static double getPtSize(double scale);


private:
	static const int basePtSz = 12;
	static const string defaultFont;
	static const string defaultFancyFont;

};

#endif