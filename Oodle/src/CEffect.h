#ifndef _CEFFECT_
#define _CEFFECT_

#include "CTimer.h"
#include "CTimedEvent.h"

//! Abstract base class for an effect
/*! An effect can only modify the surface given to it.
    It does not create any new surfaces.
 */
class CEffect : public CTimedEvent
{
public:

//	virtual void blit(CSurface& dest, int x, int y);     //! draw the "effected" surface

	//virtual bool hasImage() { return true; }
	//virtual CSurface* getImage() { return &fx; }

	enum Effects
	{
		BLACK,
		NUMEFFECTS
	};
protected:
	CEffect(int ticksPerFrame, bool concurrent);
};

#endif