#include "oodle.h"

// Effect code contributed by Keith Bare

CBlackEffect::CBlackEffect(int duration, bool concurrent, const CSurface &fx)
                          : CEffect(0, concurrent), duration(duration), black(fx), src(fx)
{
	black.fill(COLOR_BLACK);
	black.setAlpha(SDL_SRCALPHA, SDL_ALPHA_OPAQUE);
}

CBlackEffect* CBlackEffect::getCopy() 
{
	return new CBlackEffect(*this);
}

void CBlackEffect::draw(CSurface &s, int x, int y)
{
	// Calculate how grey it should be
	/*unsigned int tick = CEffect::ticksElapsed();
	Uint8 greyness = (Uint8) (254 * (((double)duration-tick) / duration));

	if (((double)duration-tick) <= 0)
	{
		setConcurrent();
		greyness = 0;
	}

	fx.lock();

	for (int y = 0; y < fx.getHeight(); y++)
	{
		for (int x = 0; x < fx.getWidth(); x++)
		{
			CColor pval;
			fx.getPixel(x, y, pval);

			// Don't mess with color key pixels
			if (pval.getR() != 255 || pval.getG() != 255 || pval.getB() != 255)
			{
				if (pval.getR() > greyness)
				{
					pval.setR(greyness);
				}

				if (pval.getG() > greyness)
				{
					pval.setG(greyness);
				}

				if (pval.getB() > greyness)
				{
					pval.setB(greyness);
				}

				fx.setPixel(x, y, pval);
			}
		}
	}

	fx.unlock();*/
	Uint32 tick = ticksElapsed();
	if (((double)duration-tick) <= 0)
	{
		s.blit(black, x, y);
		setConcurrent();
		return;
	}

	black.setAlpha(SDL_SRCALPHA, 255-(255*(((double)duration - tick) / duration)));
	s.blit(src, x, y);
	s.blit(black, x, y);

}
