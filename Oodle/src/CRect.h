#ifndef _CRECT_
#define _CRECT_

#include "CVector.h"
#include "SDL.h"

class CRect
{
public:
	enum Side
	{
		NONE = 0,
		BOTTOM = 1,
		TOP = 2,
		LEFT = 4,
		RIGHT = 8
	};

	CRect(double x, double y, double width, double height);

	double getX();
	double getY();
	double getWidth();
	double getHeight();
	virtual bool contains(double x, double y);
	virtual bool contains(CVector v);
	virtual bool checkLeft(double x);
	virtual bool checkRight(double x);
	virtual bool checkTop(double y);
	virtual bool checkBottom(double y);

	bool operator!=(CRect b);
	bool operator==(CRect b);

	void setWidth(double width) { CRect::width = width; }
	void setHeight(double height) { CRect::height = height; }
private:
	double x, y, width, height;
};

#endif