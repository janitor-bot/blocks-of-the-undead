#ifndef _CSPRITE_
#define _CSPRITE_

#include <map>
#include <list>
#include "CAnimation.h"
#include "CVector.h"
#include "CBounds.h"
#include "CEffect.h"
#include "CTimer.h"
#include "SDL.h"
#include "CParticle.h"
using namespace std;

//! Class to handle an animated (or static) sprite
class CSprite : public CParticle
{
public:
	CSprite(CVector v, CVector pos, const string &fileName, const string &animName, const CRect& b, bool alpha = false, bool nopath = false);
	CSprite(CVector v, CVector pos, const string &fileName, const string &animName, const CRect& b,
	         int width, int height, int padding, int speed, bool alpha = false, bool nopath = false);
	CSprite(CVector v, CVector pos, CSurface* s, const string &animName);
	CSprite(const CSprite& surf);
	virtual ~CSprite();

	const CSprite& operator=(const CSprite &s);

	virtual void update();              //! Updates the position of this sprite based on velocity and frame-per-second
	void draw(CSurface &surface);       //! Asks sprite to draw itself
	
	CAnimation* loadAnimation(const string &fileName, const string &name, bool alpha = false, bool nopath=false);   //! Loads a staic animation into this sprite from fileName
	CAnimation* loadAnimation(const string &fileName, const string &name, int width, int height, int padding, int speed, bool alpha = false, bool nopath=false);
	CAnimation* addAnimation(const string &name, CAnimation* a);
	void setAnimation(const string &name);      //! Set currentAnimation to animations[name]
	void appendEffect(int duration, CEffect::Effects effect);                //! Apply given effect to this sprite's current animation
	void setScale(double factor);
	void setAlpha(Uint32 flags, Uint8 alpha);
	void resetScale();
	void setRot(double rad);
	void resetRot();

	virtual CParticle::BoundaryInfo inBoundaryHoriz(double x);
	virtual CParticle::BoundaryInfo inBoundaryVert(double y);

	Uint32 getHeight() { return (int)(getCurrentFrame()->getHeight() * scale); }
	Uint32 getWidth() { return (int)(getCurrentFrame()->getWidth() * scale); }
	CSurface* getCurrentFrame(bool orEffect = false);
	CAnimation* getCurrentAnimation() { return currentAnimation; }

	virtual CSprite* getClone();
	virtual CSprite* getClone(CVector vel, CVector pos);

protected:
	CSprite(const CSprite& sprite, bool clone);          //! Construct a clone of given sprite if clone==true
	CSprite(const CSprite& sprite, bool clone, CVector vel, CVector pos);  //! Construct a clone of given sprite if clone==true
	CSprite(CVector v, CVector p);                                           //! If this constructor is used, be _sure_ to add an animation called 'static'!
	CEffect* effect;                     //! 
	double scale;
	double rot;

private:
	map<string,CAnimation*> animations;  //! map that holds all of this sprite's animations.  each animation name must be unique
	CAnimation* currentAnimation;        //! Current animation that this sprite is playing
	bool isClone;                        //! Is this a clone?  i.e. don't free surface upon deletion
};

#endif
