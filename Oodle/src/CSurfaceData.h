#ifndef _CSURFACEDATA_
#define _CSURFACEDATA_

#include "SDL.h"

class CSurfaceData
{
public:
	CSurfaceData(SDL_Surface* s) : refCount(1), s(s) { }
	CSurfaceData(CSurfaceData &surf) { #error Can't copy SurfaceData }
	const CSurfaceData& operator=(const CSurfaceData &s) { #error Can't copy SurfaceData }

	SDL_Surface* getSDLSurface() { return s; }
	void release() { if ((--refCount) == 0) SDL_FreeSurface(s); }

private:
	int refCount;
	SDL_Surface* s;
};

#endif