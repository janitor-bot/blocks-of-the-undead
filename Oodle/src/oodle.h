#ifndef _OODLE_
#define _OODLE_

/*#include <vector>
#include <list>
#include <map>

#include <ctime>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <sstream>
#include <fstream>*/
#include <string>
using namespace std;

#include "CAnimation.h"
#include "CImgLoader.h"
#include "CSDLGame.h"
#include "CSimpleGame.h"
#include "CSprite.h"
#include "CSurface.h"
#include "CVector.h"
#include "exceptions.h"
#include "resource.h"
#include "utils.h"
#include "enums.h"
#include "CRect.h"
#include "CBounds.h"
#include "CEffect.h"
#include "CBlackEffect.h"
#include "CReverseBlackEffect.h"
#include "CFadeToLayerEffect.h"
#include "CPoint.h"
#include "CTimer.h"
#include "CParticle.h"
#include "CPixel.h"
#include "CText.h"
#include "CGfxPrimitives.h"
//#include "CStatusDisplay.h"
#include "CMenu.h"
#include "CTextManager.h"
#include "CSound.h"
#include "CSoundSystem.h"

#include "CGotoEvent.h"
#include "CPauseEvent.h"
#include "CResizeEvent.h"
#include "CGotoAnimChangeEvent.h"
#include "CRandomVelEvent.h"
#include "CExplodeEffect.h"
#include "CSpriteExplodeEffect.h"
#include "CPixelExplodeEffect.h"
#include "CSyncBlockerEvent.h"
#include "CRotateEffect.h"
#include "CRemoveSeqEvent.h"
#include "CPlayAnimationEvent.h"
#include "CPlaySoundEvent.h"

#include "CBarndoorsTransition.h"

#include "SDL_rotozoom.h"
#include "SDL_gfxPrimitives.h"

#endif
