#ifndef _CPOINT_
#define _CPOINT_

class CPoint
{
public:
	CPoint(double x, double y) : x(x), y(y) {}
	double getX() { return x; }
	double getY() { return y; }

private:
	double x, y;
};

#endif