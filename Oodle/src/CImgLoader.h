#ifndef _CIMGLOADER_
#define _CIMGLOADER_

#include <string>
#include <list>
#include "SDL_image.h"
#include "SDL.h"

using namespace std;

//! Class with static methods that load images into SDL surfaces
class CImgLoader
{
public:
	static CSurface* loadImage(const string& fileName, bool alpha = false, bool nopath = false);
	//! load in either a BIGASS bitmap
	/*! Loads a list of surfaces with frames from a bigass bitmap with the specified properties
	    \param fileName Name of the file to load frames from
		\param width    Width of each frame in the file
		\param height   Height of each frame in the file
		\param padding  Number of pixels surrounding each frame
		
		\exception ex_sdlImageLoadFailed { Call to IMG_Load failed to return a pointer to a surface }
		\exception ex_illegalArguments { width, height, or padding incorrect for given file }
	*/
	static list<CSurface*> loadBigassBitmap(const string& fileName, int width, int height, int padding = 0, bool alpha = false, bool nopath = false);

	enum GraphicFileType
	{
		STATIC,   // single file
		BIGASS,   // multiple images in single file
		NUMBERED  // multiple files in a numbered sequence
	};

	static string getPath();

private:
};

#endif
