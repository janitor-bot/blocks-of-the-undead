#ifndef _CMENU_
#define _CMENU_

#include "CSurface.h"
#include "CVector.h"
#include "CSprite.h"
#include "CMenuButton.h"

class CMenu
{
public:
	~CMenu();
	CMenu(list<CMenuButton*> &buttons, CSprite* cursor, CSurface* background);
	CMenu(list<string> &text, list<CSprite*> &icons, const CVector &pos, CSprite* cursor, CSurface* background);
	
	void draw(CSurface &dest);
	void update();

private:
	list<CSprite*> bgSprites;
	list<CMenuButton*> buttons;
	CSurface* background;
	CSprite* cursor;
};

#endif