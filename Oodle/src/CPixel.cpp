#include "oodle.h"

CPixel::CPixel(CVector v, CVector p, const CColor &c)
	: CParticle(v, p), color(c), lock(1)
{
}

CPixel::CPixel(CVector v, CVector p, const CColor &c, const CRect &r)
	: CParticle(v, p, r), color(c), lock(1)
{
}

CPixel::CPixel(const CPixel &p)
	: CParticle(p), color(p.color), lock(1)
{
}

CPixel::CPixel(const CPixel &p, const CColor &c)
	: CParticle(p), color(c), lock(1)
{
}

const CPixel &CPixel::operator=(const CPixel &p)
{
	if (&p != this)   // don't allow assignment to self
	{
		velocity = p.velocity;
		pos = p.pos;
		moving = p.moving;
		timer = p.timer;
		box = p.box;
		color = p.color;
		lock = p.lock;
	}

	return *this;
}

void CPixel::draw(CSurface &surface)
{
	CTimer timer;
	try
	{
		// Only draw if the pixel will end up on the surface; off the
		// surface causes exceptions
		if (surface.onSurface(pos))
		{
			if (lock)
			{
				surface.lock();
			}

			surface.setPixel((int) pos.getX(), (int) pos.getY(),
					 color);

			if (lock)
			{
				surface.unlock();
			}
		}
	}
	catch (ex_Generic e)
	{
		_ADDTRACEINFO(e, "CPixel", "draw",
			      "surface = " << (void *) &surface);
		throw e;
	}
//	cout << "draw one pixel: " << timer.getElapsed() << endl;
}

void CPixel::lockBeforeDraw(bool l)
{
	lock = l;
}

const CColor &CPixel::getColor()
{
	return color;
}

void CPixel::setColor(const CColor &c)
{
	color = CColor(c);
}
