#include "oodle.h"

CTimer::CTimer()
         : period(0), startTime(SDL_GetTicks())
{
}

CTimer::CTimer(unsigned int period)
         : period(period), startTime(SDL_GetTicks())
{
}

/*CTimer::CTimer(const CTimer &t)
              : period(t.period), startTime(SDL_GetTicks())
{
}*/

bool CTimer::isRinging()
{
	if (period > 0 && getElapsed() > period)
	{
	//	unsigned int leftover = startTime % period;
		reset();
	//	startTime += leftover;
		return true;
	}
	return false;
}

unsigned int CTimer::getElapsed()
{
	return (SDL_GetTicks() - startTime);
}

unsigned int CTimer::getElapsedReset()
{
	unsigned int e = getElapsed();
	reset();
	return e;
}

void CTimer::setPeriod(unsigned int p)
{
	period = p;
}

void CTimer::reset()
{
	startTime = SDL_GetTicks();
}
