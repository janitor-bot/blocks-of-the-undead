#include "oodle.h"

CSound::CSound(const string &snd)
{
	string file = CImgLoader::getPath() + "snd/" + snd;
	const char* f2  = file.c_str();
	data = Mix_LoadWAV( f2 );

	if (!data)
		_THROWEX(ex_sdl, "Failed to load sound", "CSound", "CSound", "snd = " << snd);
}

CSound::~CSound()
{
	Mix_FreeChunk(data);
}

void CSound::setChannel(int c)
{
	chan = c;
}

int CSound::getChannel() const
{
	return chan;
}

Mix_Chunk* CSound::getChunk() const
{
	return data;
}