#include "oodle.h"

CMenu::CMenu(list<CMenuButton*> &buttons, CSprite* cursor, CSurface* background)
             : background(background), cursor(cursor)
{
	FOREACH(list<CMenuButton*>, buttons, i, buttons.push_back((*i)));
}

CMenu::~CMenu()
{
	delete background;
	delete cursor;
}

CMenu::CMenu(list<string> &text, list<CSprite*> &icons, const CVector &pos, CSprite* cursor, CSurface* background)
             : background(background), cursor(cursor)
{
	if (text.size() != icons.size())
	{
		_THROWEX(ex_illegalArguments, "Size of button names != size of icons", "CMenu", "CMenu", 
		           "text = {" << &text << "}, icons = {" << &icons << "}, pos = " 
				   << pos << ", background = {" << background << "}");
	}

	int padding = 10;
	CVector lastPos = pos - CVector(0, padding);

	list<string>::const_iterator i1 = text.begin();
	for (list<CSprite*>::const_iterator i2 = icons.begin(); i2 != icons.end(); ++i2)
	{
		CMenuButton* tmp = new CMenuButton( *i2, COLOR_RED, COLOR_BLACK, *i1, lastPos + CVector(0, padding));
		buttons.push_back( tmp );

		lastPos += CVector(0, tmp->getHeight());
	}
}

void CMenu::draw(CSurface &dest)
{
	dest.blit(*background, 0,0);
	FOREACH(list<CMenuButton*>, buttons  , i, (*i)->draw(dest));
	FOREACH(list<CSprite*>    , bgSprites, i, (*i)->draw(dest));
	cursor->draw(dest);
}

void CMenu::update()
{
	FOREACH(list<CMenuButton*>, buttons  , i, (*i)->update());
	FOREACH(list<CSprite*>    , bgSprites, i, (*i)->update());
	cursor->update();
}
