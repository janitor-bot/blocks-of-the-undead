#ifndef _CEVENTDATA_
#define _CEVENTDATA_

//! Base class for data transmission between Events
class CEventData
{
public:
	virtual CEventData* getCopy() = 0;
};

#endif