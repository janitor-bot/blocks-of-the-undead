#ifndef _CPAUSEEVENT_
#define _CPAUSEEVENT_

#include "CTimedEvent.h"

class CPauseEvent : public CTimedEvent
{
public:
	CPauseEvent(int time, bool c = false) : CTimedEvent(time, c) {}

	void doUpdate() { if (updateTime()) { setDone(); } }
	virtual CPauseEvent* getCopy() { return new CPauseEvent(*this); }
};

#endif