#ifndef _CLOADLEVELEVENT_
#define _CLOADLEVELEVENT_

#include "CSequencedEvent.h"

class CLoadLvlEvent : public CSequencedEvent
{
public:
	CLoadLvlEvent(CPlayingField &f, int stage, int index) : CSequencedEvent(true), f(f), stage(stage), index(index) {}
	CLoadLvlEvent* getCopy() { return new CLoadLvlEvent(*this); }
	void doUpdate() { if (!isDone()) { cout << "the eagle has landed!!!!!!!!" << endl; f.load(stage, index); setDone();} }

private:
	CPlayingField &f;
	int stage, index;
};

#endif