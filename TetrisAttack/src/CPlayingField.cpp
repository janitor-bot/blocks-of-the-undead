#include "tetris.h"

CPlayingField::CPlayingField(CVector pos, CBlockSet& set, CTetrisTheme* t, int speed)
                             : pos(pos), grav(SDL_GetTicks()), rect(CRect(pos.getX(), pos.getY(), 0, 0)),
							   cursor(0,0, FIELD_WIDTH, FIELD_HEIGHT, pos),
							   moveUpTimer(speed), moveUpOffset(0), blockSet(set), moves(1), numBlocks(0),
							   lStage(0), lIndex(0), gameOver(false), gameWon(false), txtColor(),
							   theme(t),
							   movesTxt(" ", CColor(), CTextManager::getDefaultFont( CTextManager::PLAIN ), CTextManager::getPtSize(2), CText::BOLD),
							   stageTxt(" ", CColor(), CTextManager::getDefaultFont( CTextManager::PLAIN ), CTextManager::getPtSize(2), CText::BOLD),
							   gameOverTxt("Press any key to retry level", CColor(255), CTextManager::getDefaultFont( CTextManager::PLAIN ), CTextManager::getPtSize(1.2), CText::BOLD)
{
	//memset(blocks, 0, (FIELD_HEIGHT-1) * (FIELD_WIDTH-1) * sizeof(CBlock*));
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			blocks[x][y] = NULL;
		}
	}
	/*for (int i = 0; i < FIELD_WIDTH; i++)
	{
		addBlock(i, FIELD_HEIGHT - 1, static_cast<BlockType>(rand()%6));
		blocks[i][FIELD_HEIGHT-1]->setActive(false);
	}*/
}

CPlayingField::~CPlayingField()
{
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			delete blocks[x][y];
		}
	}
//	delete blockSet;
}

bool CPlayingField::checkBounds(int x, int y)
{
	return (x < FIELD_WIDTH && x >= 0 && y < FIELD_HEIGHT && y >= 0);
}

bool CPlayingField::allActive()
{
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			if (blockExists(x,y) && !blocks[x][y]->isActive())
			{
				return false;
			}
		}
	}
	return true;
}

bool CPlayingField::allFallen()
{
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT-1; y++)
		{
			if (blockExists(x,y) && !blockExists(x,y+1))
			{
				return false;
			}
		}
	}
	return true;
}

void CPlayingField::draw(CSurface& s)
{
	/*for (int i = 0; i < FIELD_WIDTH; i++)
	{
		blocks[i][FIELD_HEIGHT-1]->setActive(false);
	}*/
	//moveUp();
	//cout << "Execute applyGravity()" << endl;
	//applyGravity();								// give floating blocks velocity downwards
	s.setClippingRect(CRect(pos.getX(), pos.getY(), width*FIELD_WIDTH, height*FIELD_HEIGHT));
	for (int x = FIELD_WIDTH-1; x >= 0; x--)
	{
		for (int y = FIELD_HEIGHT-1; y >= 0; y--)
		{
			if (blocks[x][y] != NULL)
			{
				blocks[x][y]->update();
			}
		}
	}
	checkBlocks();								// check for blocks to be cleared

	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			if (blocks[x][y] != NULL)
			{
				//blocks[x][y]->setPosRel(0, height - moveUpOffset);
				blocks[x][y]->draw(s);
				//blocks[x][y]->setPosRel(0, -height + moveUpOffset);
			}
		}
	}
	s.resetClippingRect();

	for (list<CBlock*>::iterator i = freeBlocks.begin(); i != freeBlocks.end(); ++i)
	{
		//(*i)->setPosRel(0, height - moveUpOffset);
		(*i)->draw(s);
		//(*i)->setPosRel(0, -height + moveUpOffset);
	}
	//cursor.setPosRel(0, height - moveUpOffset);
	cursor.update();
	cursor.draw(s);
	if (allActive() && allFallen())
	{
		if (isEmpty())
		{
			if (gameOver)
			{
				//CText t("GAME OVER", CTextManager::getDefaultFont(), 12);
				//t.setPos(200,200);
				//t.draw(s);
				gameOverTxt.setPos(pos + CVector((width*FIELD_WIDTH-gameOverTxt.getWidth())/2, (height*FIELD_HEIGHT-gameOverTxt.getHeight())/2));
				gameOverTxt.draw(s);
			}
			else
			{
				if (lIndex < 6)
					load(lStage, lIndex+1);
				else
				{
					if (lStage < 3)
					{
						load(lStage+1,1);
					}
					else
					{
						gameWon = true;
					}
				}
			}
		}
		else if (moves <= 0)
		{
			// game over
			gameOver = true;
			//CSprite *death = new CSprite(ZERO_VECTOR, ZERO_VECTOR, "deadblock.png", "static", CRect(0,0,0,0));
			for(int x = 0; x < FIELD_WIDTH; x++)
			{
				for(int y = 0; y < FIELD_HEIGHT; y++)
				{
					if (blockExists(x,y))
					{
						CSequencedEvent *e = new CBlockDeathEffect( *blocks[x][y], CImgLoader::loadImage("deadblock.png") );
						blocks[x][y]->appendEvent( e );
                        e->update();						
						blocks[x][y]->appendEvent( new CPauseEvent(1) );
					}
				}
			}

			CSyncBlockerEvent *e = new CSyncBlockerEvent(0);
			e->setTotal( addEventAllBlocks( e ) );
			
			for(int x = 0; x < FIELD_WIDTH; x++)
			{
				for(int y = 0; y < FIELD_HEIGHT; y++)
				{
					if (blockExists(x,y))
					{
						blocks[x][y]->appendEvent( new CPauseEvent(1000) );
						blocks[x][y]->appendEvent( new CRemoveBlockEvent(blocks[x][y], false) );
					}
				}
			}
			
			//addEventAllBlocks( new CLoadLvlEvent( *this, lStage, lIndex) );
		//	load(lStage, lIndex);
		}
	}
	movesTxt.draw(s);
	stageTxt.draw(s);
	//cursor.setPosRel(0, -height + moveUpOffset);
}

int CPlayingField::addEventAllBlocks(CSequencedEvent *e)
{
	int n = 0;
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			if (blockExists(x,y))
			{
				e->plusRef();
				blocks[x][y]->appendEvent(e);
				n++;
			}
		}
	}
	return n;
}

void CPlayingField::applyGravity()
{   // this is a little inefficient, i think
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT - 2; y++)
		{
			if (blocks[x][y] != NULL && blocks[x][y]->isActive() && blocks[x][y]->getVel().getX() == 0 && blocks[x][y+1] == NULL)   // nothing holding up this column, so give it downward velocity (unless we've already given it one)
			{
				cout << "applying gravity to {" << blocks[x][y] << "}" << endl;
				// move block into the null location
				blocks[x][y+1] = blocks[x][y];
				blocks[x][y+1]->moveRelBoardCords(0, 1, 300);
				blocks[x][y+1]->update();
				blocks[x][y] = NULL;
				int i = 1;
				
				if (y > 0)
				{
					CBlock* ptr = blocks[x][y-i];
					while(ptr != NULL && y-i < FIELD_HEIGHT - 2 && y-i >= 0)       // go up column until we find a null block, moving the blocks down
					{
						blocks[x][y-i+1] = blocks[x][y-i];
						blocks[x][y-i]->moveRelBoardCords(0, 1, 300);
						blocks[x][y-i] = NULL;
						i++;
						ptr = blocks[x][y-i];
					}
				}
				y = y+i;
			}
		}
	}
}

void CPlayingField::moveBlock(int x1, int y1, int x2, int y2)
{
	 if (blockExists(x1, y1) && checkBounds(x2,y2))
	 { 
		 delete blocks[x2][y2];
		 blocks[x2][y2] = blocks[x1][y1];
		 blocks[x1][y1] = NULL;
	 }
}

void CPlayingField::moveUp()
{
	if (moveUpTimer.isRinging())
	{
		if (moveUpOffset > height)
		{
			// move all blocks up one
			for (int x = 0; x < FIELD_WIDTH; x++)
			{
				for (int y = 0; y < FIELD_HEIGHT; y++)
				{
					if (blockExists(x, y) && !blockExists(x,y-1))
					{
						if (y == 0)
						{
								delete blocks[x][y];
								blocks[x][y] = NULL;
						}
						else if (y == FIELD_HEIGHT -1)
						{
							blocks[x][y]->setActive(true);
							blocks[x][y-1] = blocks[x][y];
							blocks[x][y-1]->moveRelBoardCords(0, -1);
							blocks[x][y] = NULL;
						}
						else
						{
							if ((int)blocks[x][y]->getVel().getY() == 0)
							{
								blocks[x][y-1] = blocks[x][y];
								blocks[x][y-1]->moveRelBoardCords(0, -1);
								blocks[x][y] = NULL;
							}
							else
							{
							}
						}
					}
				}	
			}

			/*for (int i = 0; i < FIELD_WIDTH; i++)
			{
				addBlock(i, FIELD_HEIGHT - 1, static_cast<BlockType>(rand()%6));
				blocks[i][FIELD_HEIGHT-1]->setActive(false);
			}*/
			moveUpOffset = 0;
			moveCursor(0, -1, 0);
		}
		moveUpOffset++;
	}
}

void CPlayingField::clear()
{
	numBlocks = 0;
	for(int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			delete blocks[x][y];
			blocks[x][y] = NULL;
		}
	}
}

void CPlayingField::setMoves(int m)
{
	moves = m;
	ostringstream ss;
	ss << "Moves Remaining:  " << m;
	movesTxt.setText(ss.str(),CText::BOLD, txtColor);
	movesTxt.setPos(10,10);
}

void CPlayingField::load(int stage, int index)
{
	if (stage == 2) txtColor = COLOR_WHITE;
	if (stage == 1) txtColor = COLOR_BLACK;

	ostringstream ss;
	ss << CImgLoader::getPath() << "lvls/" << stage << "/" << index << ".txt";
	ifstream lvl( ss.str().c_str() );
	int x = 0, y = 0;
	
	if (!lvl)
	{
		string s =  ss.str();
		_THROWEX(ex_Generic, "Failed to load lvl (" << stage << ", " << index << ") from file "
		<< s, "CPlayingField", "load", "stage = " << stage << ", index = " << index);
	}


	clear();
	lvl >> moves;
	setMoves(moves);
	gameOver = false;
	gameWon = false;
	ostringstream ss2;
	ss2 << "Level: " << stage << "-" << index;
	stageTxt.setText(ss2.str(), CText::NONE, txtColor);
	stageTxt.setPos(10 , movesTxt.getHeight() + 10);
	lStage = stage;
	lIndex = index;

	while(!lvl.eof())
	{
		char c = lvl.get();
		if (! (c == '\n' || c == '\r' || c == ' ' || c == '\t') )
		{
			switch(c - 47)
			{
			case 0:
				// do nothing
				break;
			case 1:
				addBlock(x,y,true,BLOCK_1);
				break;
			case 2:
				addBlock(x,y,true,BLOCK_2);
				break;
			case 3:
				addBlock(x,y,true,BLOCK_3);
				break;
			case 4:
				addBlock(x,y,true,BLOCK_4);
				break;
			case 5:
				addBlock(x,y,true,BLOCK_5);
				break;
			case 6:
				addBlock(x,y,true,BLOCK_6);
				break;
			}
			if (blockExists(x,y))
			{
				blocks[x][y]->appendEvent( new CReverseBlackEffect(1000, false, *blocks[x][y]->getCurrentFrame()) );
			}

			if ( ! (x+1 < FIELD_WIDTH) )
			{
				x = 0;
				y++;
			}
			else
			{
				x++;
			}
		}
	}
	lvl.close();
}
	
bool CPlayingField::isEmpty()
{
	return numBlocks == 0;
}

void CPlayingField::removeBlockEffect(CBlock* ptr, CSyncBlockerEvent* sync)
{
	ptr->setActive(false);
	ptr->setRect(CRect(0,0,0,0));

	//ptr->appendEffect(1000, CEffect::BLACK);
	CSequencedEvent *e = new CBlackEffect(1000, false, *ptr->getCurrentFrame());
	ptr->appendEvent( e );
	ptr->appendEvent(new CPauseEvent(200 * (sync->getRef() - 1)) );
	ptr->appendEvent(new CRemoveSeqEvent( e, *ptr) );
	//CRect(pos.getX(), pos.getY(), width*FIELD_WIDTH, height*FIELD_HEIGHT)
	ptr->appendEvent(new CPlaySoundEvent(explode, CSoundPlayer, true) );
	ptr->appendEvent(new CSpriteExplodeEffect(*ptr, 3000, 100, 250, CRect(0,0,0,0), 4, 4, false) );
	ptr->appendEvent( sync );
	ptr->appendEvent( new CRemoveBlockEvent(ptr, false) );
	freeBlocks.push_back( ptr );

}

int CPlayingField::getBlockX(CBlock *b)
{
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_WIDTH; y++)
		{
			if (blocks[x][y] && blocks[x][y] == b)
				return x;
		}
	}
	return 0;  // should throw exception here
}

int CPlayingField::getBlockY(CBlock *b)
{
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_WIDTH; y++)
		{
			if (blocks[x][y] == b)
				return y;
		}
	}
	return 0;   // should throw exception here
}

void CPlayingField::removeList( list<CPoint> &toRemove, CSyncBlockerEvent* sync )
{
	for(list<CPoint>::iterator i = toRemove.begin(); i != toRemove.end(); ++i)
	{
		CBlock* ptr = blocks[(int)(*i).getX()][(int)(*i).getY()];
		if (ptr->isActive())
		{
			sync->plusRef();
			removeBlockEffect(ptr, sync);
		}
	}
	sync->minusRef();
//delete sync;
}

void CPlayingField::checkBlocks()
{
	for (int x = 0; x < FIELD_WIDTH; x++)
	{
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			if (blockExists(x,y))
			{
				if (blocks[x][y]->deleteMe())
				{
					removeBlock(x,y);
				}
				else
				{
					//CSyncBlockerEvent* sync = new CSyncBlockerEvent(
					bool removedBlocks = false;
					CBlock* ptr = blocks[x][y];
					int i = 0;
					int posY = 1;
					int negY = 1;
					int posX = 1;
					int negX = 1;

					i = 1;
					list<CPoint> toRemove;
					list<CPoint> tmp;
					// first go up
					while(ptr != NULL && blockExists(x, y-i) && ptr->sameType(*blocks[x][y-i]) && ptr->isActive() && blocks[x][y-i]->isActive())
					{
						tmp.push_back(CPoint(x, y-i));
						negY++;
						i++;
					}
					i--;
					if (negY >= 3)
					{
						toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();

						int y1 = y-i;
						i = 1;
						while(ptr != NULL && blockExists(x-i, y1) && ptr->sameType(*blocks[x-i][y1]) && ptr->isActive() && blocks[x-i][y1]->isActive())
						{
							tmp.push_back(CPoint(x-i, y1));
							negX++;
							i++;
						}
						if (negX >= 2)
							toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();

						i = 1;
						while(ptr != NULL && blockExists(x+i, y1) && ptr->sameType(*blocks[x+i][y1]) && ptr->isActive() && blocks[x+i][y1]->isActive())
						{
							tmp.push_back(CPoint(x+i, y1));
							posX++;
							i++;
						}
						if (posX >= 2)
							toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();
					}

					tmp.clear();

					ptr = blocks[x][y];
					i = 1;
					// next go down
					while(ptr != NULL && blockExists(x, y+i) && ptr->sameType(*blocks[x][y+i]) && ptr->isActive() && blocks[x][y+i]->isActive())
					{
						tmp.push_back(CPoint(x, y+i));
						posY++;
						i++;
					}
					i--;
					if (posY >= 3)
					{
						toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();

						int y1 = y+i;
						i = 1;
						while(ptr != NULL && blockExists(x-i, y1) && ptr->sameType(*blocks[x-i][y1]) && ptr->isActive() && blocks[x-i][y1]->isActive())
						{
							tmp.push_back(CPoint(x-i, y1));
							negX++;
							i++;
						}
						if (negX >= 2)
							toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();

						i = 1;
						while(ptr != NULL && blockExists(x+i, y1) && ptr->sameType(*blocks[x+i][y1]) && ptr->isActive() && blocks[x+i][y1]->isActive())
						{
							tmp.push_back(CPoint(x+i, y1));
							posX++;
							i++;
						}
						if (posX >= 2)
							toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();

					}
					tmp.clear();

					ptr = blocks[x][y];
					i = 1;
					// now go left
					while(ptr != NULL && blockExists(x-i, y) && ptr->sameType(*blocks[x-i][y]) && ptr->isActive() && blocks[x-i][y]->isActive())
					{
						tmp.push_back(CPoint(x-i, y));
						negX++;
						i++;
					}
					if (negX >= 3)
					{
						toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();

						int x1 = x-i;
						i = 1;
						// go up
						while(ptr != NULL && blockExists(x1, y-i) && ptr->sameType(*blocks[x1][y-i]) && ptr->isActive() && blocks[x1][y-i]->isActive())
						{
							tmp.push_back(CPoint(x1, y-i));
							negY++;
							i++;
						}
						if (negY >= 2)
							toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
					}
					tmp.clear();

					ptr = blocks[x][y];
					i = 1;
					// finally go right
					while(ptr != NULL && blockExists(x+i, y) && ptr->sameType(*blocks[x+i][y]) && ptr->isActive() && blocks[x+i][y]->isActive())
					{
						tmp.push_back(CPoint(x+i, y));
						posX++;
						i++;
					}
					i--;
					if (posX >= 3)
					{
						toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();

						int x1 = x+i;
						i =1;
						// go up
						while(ptr != NULL && blockExists(x1, y-i) && ptr->sameType(*blocks[x1][y-i]) && ptr->isActive() && blocks[x1][y-i]->isActive())
						{
							tmp.push_back(CPoint(x1, y-i));
							negY++;
							i++;
						}
						if (negY >= 2)
							toRemove.insert(toRemove.end(), tmp.begin(), tmp.end());
						tmp.clear();
					}

					if (toRemove.size() > 0)
					{
						toRemove.push_back(CPoint(x,y));
						removeList(toRemove, new CSyncBlockerEvent(toRemove.size()));
					}

		//			clearedBlockLocations.push_back( CBlockLoc(x, y, negX, posX, negY, posY) );  // x and y are the center of the cleared block, the other numbers define its shape
				}
			}
		}
	}
}

void CPlayingField::swap()
{

	if (cursor.getBoardX() + cursor.getBoardWidth() > FIELD_WIDTH || cursor.getBoardX() < 0) // something bad happened, the cursor is outside of the board
	{
		// throw some kind of exception
		return;
	}
	if (cursor.getBoardY() + cursor.getBoardHeight() > FIELD_HEIGHT || cursor.getBoardY() < 0) // something bad happened, the cursor is outside of the board
	{
		// throw some kind of exception
		return;
	}

	CBlock* tmp = blocks[ cursor.getBoardX() ][ cursor.getBoardY() ];   // swap the block (TODO: some kind of effect)
	CBlock* tmp2 = blocks[ cursor.getBoardX() + 1 ][ cursor.getBoardY() ];   // swap the block (TODO: some kind of effect)
	if (! (tmp || tmp2) )
	{
		return;
	}
	if ((tmp && !tmp->isActive()) || (tmp2 && !tmp2->isActive()))
	{  // bail out if either are inactive
		return;
	}

	if(theme)
	{
		CSoundPlayer.playSound( theme->getSwapSound() );
	}

	if (! (moves > 0) )
		return;
	setMoves(moves - 1);

	if (tmp && tmp2)
	{
		blocks[ cursor.getBoardX() ][ cursor.getBoardY() ] = blocks[ cursor.getBoardX() + 1 ][ cursor.getBoardY() ];
		blocks[ cursor.getBoardX() + 1 ][ cursor.getBoardY() ] = tmp;
		tmp->moveRelBoardCords(1,0,700);
		tmp2->moveRelBoardCords(-1,0,700);
		/*tmp->appendEvent( new CGotoAnimChangeEvent(*tmp) );
		tmp2->appendEvent( new CGotoAnimChangeEvent(*tmp2) );*/
	}
	else if (tmp && !tmp2)
	{
		blocks[ cursor.getBoardX() + 1 ][ cursor.getBoardY() ] = tmp;
		blocks[ cursor.getBoardX() ][ cursor.getBoardY() ] = NULL;
		tmp->moveRelBoardCords(1,0,700);
		tmp->appendEvent( new CGotoAnimChangeEvent(*tmp) );
		if (!blockExists(cursor.getBoardX()+1, cursor.getBoardY()+1))
			tmp->appendEvent( new CPauseEvent(200) );
	}
	else if (!tmp && tmp2)
	{
		blocks[ cursor.getBoardX() ][ cursor.getBoardY() ] = tmp2;
		blocks[ cursor.getBoardX() + 1 ][ cursor.getBoardY() ] = NULL;
		tmp2->moveRelBoardCords(-1,0,700);
		tmp2->appendEvent( new CGotoAnimChangeEvent(*tmp2) );
		if (!blockExists(cursor.getBoardX(), cursor.getBoardY()+1))
			tmp2->appendEvent( new CPauseEvent(200) );
	}
}

void CPlayingField::appendBlockEffect(int x, int y, int duration, CEffect::Effects effect)
{
	if (blockExists(x,y))
	{
		blocks[x][y]->appendEffect(duration, effect);
	}
	else
	{
		// throw exception
	}
}

void CPlayingField::appendBlockEvent(int x, int y, CSequencedEvent* e)
{
	if (blockExists(x,y))
	{
		blocks[x][y]->appendEvent(e);
	}
}

void CPlayingField::moveCursor(int dX, int dY, double sec)
{
	cursor.moveRelBoardCords(dX, dY);
}

void CPlayingField::moveCursorAbs(int x, int y, double sec)
{
	cursor.moveBoardCords(x, y);
}

void CPlayingField::setBackground(const string& fileName)
{
//	background = CImgLoader::loadImage(fileName);
}

void CPlayingField::addBlock(int x, int y, BlockType type)
{
	addBlock(x, y, true, type);
}

void CPlayingField::addBlock(int x, int y, bool active, BlockType type)
{
	if (x < FIELD_WIDTH && y < FIELD_HEIGHT && y >= 0 && x >= 0)
	{
		delete blocks[x][y];   // okay to call delete even on null blocks
		if (blocks[x][y] == NULL) 		numBlocks++;
		blocks[x][y] = blockSet.getBlock(x, y, FIELD_WIDTH, FIELD_HEIGHT, pos, type);
		blocks[x][y]->setActive(active);
		blocks[x][y]->appendBehavior( new CApplyGravityEvent( *blocks[x][y], *this ) );
	//	blocks[x][y]->appendEvent(new CReverseBlackEffect(1000, false, *blocks[x][y]->getCurrentFrame()) );
		width = blocks[x][y]->getWidth();
		height = blocks[x][y]->getHeight();
		//blocks[x][y]->setRect(CRect(x * width + pos.getX(), y * height + pos.getY(), width+50, 50+height * FIELD_HEIGHT));
		//blocks[x][y]->setRect(CRect(0, 0, 640, 480));

		rect.setWidth(width*FIELD_WIDTH);
		rect.setHeight(height*FIELD_HEIGHT);
		/*if (moveUpOffset == 0)
			moveUpOffset = 0;*/
	}
	else
	{
		_THROWEX(ex_illegalArguments, "Block added outside allowed cords", "CPlayingField", "addBlock",
			"x = " << x << ", y = " << y << ", type = " << type);
	}
}

void CPlayingField::setBlockAnim(int x, int y, const string &name)
{
	if (blocks[x][y])
	{
		cout << "setAnimation " << name << endl;
		blocks[x][y]->setAnimation(name);
	}
}

bool CPlayingField::boardCords(CVector p, int &x, int &y)
{
	if (rect.contains(p.getX(), p.getY()))
	{
		CVector tmp = p - pos;
		x = static_cast<int> (tmp.getX()/width);
		y = static_cast<int> (tmp.getY()/height);
	}
	else
	{
		return false;
	}
	return true;
}

void CPlayingField::removeBlock(int x, int y)
{
	if (blocks[x][y])
	{
		numBlocks--;
	//	LOG("removing block at (" << x << ", " << y << ")", 5, LOG_INFO);

		blocks[x][y]->clear();

		freeBlocks.remove(blocks[x][y]);
		delete blocks[x][y];
		blocks[x][y] = NULL;
	}
}

bool CPlayingField::blockExists(int x, int y)
{
	if (x < FIELD_WIDTH && x >= 0 && y < FIELD_HEIGHT && y >= 0)
		return blocks[x][y] != NULL;
	else
		return false;
}
