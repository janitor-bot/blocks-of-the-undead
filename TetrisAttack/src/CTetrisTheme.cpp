#include "tetris.h"

CTetrisTheme::CTetrisTheme(const string &name, const string &res)
			                : background(CImgLoader::loadImage(name + "/" + res + "/background.png", false)), 
			                  blockSet(new CBlockSet(name + "/" + res + "/")), 
							  swap("swap.wav"), hit("hit.wav"),
							  cursor(CImgLoader::loadImage(name + "/" + res + "/cursor.png", true)),
							  character( new CSprite(ZERO_VECTOR, ZERO_VECTOR, name + "/" + res + "/character.png", "static", CRect(0,0,0,0), true))
{
	string file = CImgLoader::getPath() + "/img/" + name + "/" + res + "/floater.txt";
	ifstream data( file.c_str() );
	int x = 0, y = 0;
	
	if (data)
	{
		/*_THROWEX(ex_Generic, "Failed to load data file " << file, 
			"CPlayingField", "load", "name = " << name << ", res = " << res);*/
	
		int width=0, height=0, speed=0;
		int alpha = SDL_ALPHA_OPAQUE;
		data >> width;
		data >> height;
		data >> speed;
		data.close();
		floater = new CSprite(ZERO_VECTOR, ZERO_VECTOR,  "img/" + name + "/" + res + "/floater.png", "static", CRect(0,0,0,0), width, height, 0, speed, true);
	}
	else
	{
		floater = NULL;
	}

	file = CImgLoader::getPath() + "/img/" + name + "/" + res + "/character.txt";
	ifstream data2( file.c_str() );
	
	if (!data2)
	{
		_THROWEX(ex_Generic, "Failed to load data file " << file, 
			"CPlayingField", "load", "name = " << name << ", res = " << res);
	}
	
	x=0; y=0;
    data2 >> x;
	data2 >> y;
	data2.close();
	character->setPos(x,y);
}

CTetrisTheme::~CTetrisTheme()
{
	delete background;
	delete cursor;
	delete blockSet;
	delete character;
	delete floater;
}

CSurface* CTetrisTheme::getBackground()
{
	return background;
}

CBlockSet* CTetrisTheme::getBlockSet()
{
	return blockSet;
}

CSurface* CTetrisTheme::getCursor()
{
	return cursor;
}

CSprite* CTetrisTheme::getCharacter()
{
	return character;
}

CSprite* CTetrisTheme::getFloater()
{
	return floater;
}

CSound& CTetrisTheme::getSwapSound()
{
	return swap;
}