#include "tetris.h"


void CApplyGravityEvent::initialize()
{
	pause.reset();
}

void CApplyGravityEvent::doUpdate()
{
	int x = field.getBlockX(&block);
	int y = field.getBlockY(&block);

	x = block.getBoardX();
	y = block.getBoardY();

	if (block.isActive() && block.getVel().getX() == 0 && !field.blockExists( x, y + 1) && field.blockExists(x,y) && y+1 < field.FIELD_HEIGHT)
	{
		falling = true;
	/*	if (pause.getElapsed() > 200)
		{*/
			// move block into the null location
			field.moveBlock(x, y, x, y + 1);
			if (stopped)
			{
				block.appendEvent( new CPauseEvent(250) );
				block.moveRelBoardCords(0, 1, 500);
				stopped = false;
			}
			else
			{
				CSequencedEvent *e = block.moveRelBoardCords(0, 1, 500);
				if (e)
				{
					e->update();
				}
			}
	//		block.appendEvent( new CPauseIfFirstDropEvent(100) );
			//block.appendEvent( new CApplyGravityEvent( block, field ) );
			//setDone();
			pause.reset();
		//}
	}
	else if ( (int)block.getVel().getY() == 0)
	{
		if (falling == true && !stopped){
			block.setAnimation("hitanim_bottom");
			CSoundPlayer.playSound( hit );
		}
		stopped = true;
		falling = false;
	}
}
