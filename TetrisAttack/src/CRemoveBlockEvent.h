#ifndef _CREMOVEBLOCKEVENT_
#define _CREMOVEBLOCKEVENT_

#include "CBlock.h"
#include "oodle.h"

class CRemoveBlockEvent : public CSequencedEvent
{
public:
	CRemoveBlockEvent(CBlock* b, bool c) : CSequencedEvent(c), block(b) {}
	CRemoveBlockEvent* getCopy();
	
	bool hasDraw() { return true; }
	void doUpdate();

private:
	CBlock* block;
};

#endif