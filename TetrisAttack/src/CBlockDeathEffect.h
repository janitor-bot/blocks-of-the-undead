#ifndef _CBLOCKDEATHEFFECT_
#define _CBLOCKDEATHEFFECT_

#include "CSequencedEvent.h"

class CBlockDeathEffect : public CTimedEvent
{
public:
	CBlockDeathEffect(CBlock &b, CSurface *death) : CTimedEvent(50,true), death(death), b(b), count(0), x((int)b.getPos().getX()), y((int)b.getPos().getY()) {   }
	CBlockDeathEffect* getCopy() { return new CBlockDeathEffect(*this); }
	~CBlockDeathEffect() { delete death; }

	void initialize()
	{CTimedEvent::initialize(); b.appendEvent( new CFadeToLayerEffect(500, *b.getCurrentFrame(), *death, true) ); }
	void doUpdate() {
		bool t = CTimedEvent::updateTime();
		if (t && count % 2 == 0)
		{
			b.setPos(CVector(x, y+2));
			count++;
		}
		else if (t)
		{
			b.setPos(CVector(x, y-2));
			count++;
		}

		if (count > 10) { b.setPos(CVector(x,y)); setDone(); }
	}

private:
	CBlock &b;
	CSurface *death;
	int x, y;
	int count;
};

#endif