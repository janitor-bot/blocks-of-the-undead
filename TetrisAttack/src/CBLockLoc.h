#ifndef _CBLOCKLOC_
#define _CBLOCKLOC_

class CBlockLoc
{
public:
	CBlockLoc(int x, int y, int negX, int posX, int negY, int posY);    //! X and y give its center, the other values give its shape
	bool isTouching(CBlockLoc o);                                       //! Check if this block is touching another block

private:
	int x, y, negX, negY, posX, posY;
};

#endif